# Generated by Django 3.1.6 on 2021-02-11 09:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cron_job', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='job',
            name='deactivate_after_fail',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='job',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='is_authorize',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='job',
            name='notification_job_failed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='job',
            name='notification_success_after_fail',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='job',
            name='save_response',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='response',
            name='job_fail',
            field=models.BooleanField(default=False),
        ),
    ]
