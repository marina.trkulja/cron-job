from .forms import *
from .models import Job
from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user


# create cron view
@login_required(login_url='login')
def create_cron(request):
    user = get_user(request)
    form = CreateCron()
    if request.method == 'POST':
        form = CreateCron(request.POST)
        if form.is_valid():
            Job.objects.create(
                title=form.cleaned_data['title'],
                link=form.cleaned_data['link'],
                is_authorize=form.cleaned_data['is_authorize'],
                user_authorize=form.cleaned_data['username_authorize'],
                password_authorize=form.cleaned_data['password_authorize'],
                cron_expression=form.cleaned_data['custom'],
                is_active=form.cleaned_data['is_active'],
                fk_user=user,
                save_response=form.cleaned_data['job_save'],
                notification_job_failed=form.cleaned_data['job_fail'],
                notification_success_after_fail=form.cleaned_data['job_success_after_fail'],
                deactivate_after_fail=form.cleaned_data['job_deactivate_due_many_error'],
            )
            messages.success(request,'Job is successfully created')
            return redirect('dashboard')
        else:
            messages.error(request, 'Something went wrong.')
    return render(request, 'cron/create_cron.html', {'form': form, 'mode': 'Create'})


# update cron view
@login_required(login_url='login')
def update_cron(request, cronjob_id):
    user = get_user(request)
    try:
        job = Job.objects.get(id=cronjob_id)
        if not job.fk_user == user:
            messages.error(request, 'This is not your job.')
            return redirect('dashboard')
        form = CreateCron(initial={
            'title': job.title,
            'link': job.link,
            'is_authorize': job.is_authorize,
            'username_authorize': job.user_authorize,
            'password_authorize': job.password_authorize,
            'execution_type': 'custom',
            'custom': job.cron_expression,
            'is_active': job.is_active,
            'job_save': job.save_response,
            'job_fail': job.notification_job_failed,
            'job_success_after_fail': job.notification_success_after_fail,
            'job_deactivate_due_many_error': job.deactivate_after_fail,

        })

        if request.method == 'POST':
            form = CreateCron(request.POST)
            if form.is_valid():
                job.title = form.cleaned_data['title']
                job.link = form.cleaned_data['link']
                job.is_authorize = form.cleaned_data['is_authorize']
                job.user_authorize = form.cleaned_data['username_authorize']
                job.password_authorize = form.cleaned_data['password_authorize']
                job.cron_expression = form.cleaned_data['custom']
                job.is_active = form.cleaned_data['is_active']
                job.save_response = form.cleaned_data['job_save']
                job.notification_job_failed = form.cleaned_data['job_fail']
                job.notification_success_after_fail = form.cleaned_data['job_success_after_fail']
                job.deactivate_after_fail = form.cleaned_data['job_deactivate_due_many_error']
                job.save()
                messages.success(request, "Successfully updated")
                return redirect('dashboard')

    except Job.DoesNotExist:
        messages.error(request, "Job does not exist")
        return redirect("dashboard")

    return render(request, 'cron/create_cron.html', {'form': form, 'mode': 'Update'})


@login_required(login_url='login')
def dashboard_page(request):
    user = get_user(request)
    jobs = Job.objects.filter(fk_user=user)
    return render(request, 'cron/dashboard.html', {'jobs': jobs})


@login_required(login_url='login')
def delete_cron_job(request, cronjob_id):
    user = get_user(request)
    try:
        my_job = Job.objects.get(id=cronjob_id)
        if my_job.fk_user == user:
            my_job.delete()
        else:
            messages.error(request, 'This is not your Cron-job!')
    except Job.DoesNotExist:
        messages.error(request, 'Cron-job does not exist.')
    return redirect('dashboard')
