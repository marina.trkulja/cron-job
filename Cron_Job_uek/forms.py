from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms


class CreateUserForm(UserCreationForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Firstname...'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Lastname...'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Username...'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'E-Mail...'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password...'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password (again)'}))

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'



class LoginUserForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Username...'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password...'}))

    class Meta:
        model = User
        fields = ['username', 'password']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
