from django.urls import path
from . import views

urlpatterns = [
    path('cronjobs/<int:cronjob_id>', views.update_cron, name='Update Cron'),
    path('cronjobs/', views.dashboard_page, name='dashboard'),
    path('cronjobs/create', views.create_cron, name='Create Cron'),
    path('cronjobs/<int:cronjob_id>/delete', views.delete_cron_job, name='delete_job')
]
