from django import forms
from croniter import croniter

ex_type_choices = [
    ('minute', 'Every'),
    ('day', 'Every Day at'),
    ('month', 'Always on'),
    ('custom', 'Custom')
]


# Dropdown number
def create_choices(number: int):
    return [tuple([x, x]) for x in range(1, number)]


# Form fields
class CreateCron(forms.Form):
    title = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Add new title...',
                'class': 'form-control',
            },
        ),
        required=True
    )
    link = forms.CharField(
        widget=forms.URLInput(
            attrs={
                'placeholder': 'Add new link...',
                'class': 'form-control',
            }
        ),
        required=True
    )
    is_authorize = forms.BooleanField(required=False)
    username_authorize = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Add Username..',
                'class': 'form-control',
            }
        ),
        required=False
    )
    password_authorize = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Password..',
                'class': 'form-control',
            }
        ),
        required=False
    )
    execution_type = forms.ChoiceField(widget=forms.RadioSelect, choices=ex_type_choices, required=True)
    minute_minutes = forms.ChoiceField(choices=create_choices(60), required=False)
    day_hours = forms.ChoiceField(choices=create_choices(24), required=False)
    day_minutes = forms.ChoiceField(choices=create_choices(60), required=False)
    month_day = forms.ChoiceField(choices=create_choices(31), required=False)
    month_hours = forms.ChoiceField(choices=create_choices(24), required=False)
    month_minutes = forms.ChoiceField(choices=create_choices(60), required=False)
    custom = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        ),
        max_length=20,
        required=False
    )
    job_fail = forms.BooleanField(label='The job fails', required=False)
    job_success_after_fail = forms.BooleanField(label='The Job is successfully executed after previous failure',
                                                required=False)
    job_deactivate_due_many_error = forms.BooleanField(
        label='the Job gets automatically deactivated due too many errors', required=False)
    job_save = forms.BooleanField(label='save answers', required=False)
    is_active = forms.BooleanField(label='Active', required=False)

    # Cron Expressions check
    def clean(self):
        if self.cleaned_data['execution_type'] == 'minute':
            ex = f"*/{self.cleaned_data['minute_minutes']} * * * *"
        elif self.cleaned_data['execution_type'] == 'day':
            ex = f"{self.cleaned_data['day_minutes']} {self.cleaned_data['day_hours']} * * *"
        elif self.cleaned_data['execution_type'] == 'month':
            ex = f"{self.cleaned_data['month_minutes']} {self.cleaned_data['month_hours']} {self.cleaned_data['month_day']} * *"

        else:
            ex = '{}'.format(self.cleaned_data['custom'])
        if not croniter.is_valid(ex):
            raise forms.ValidationError('Invalid Cron-Expression!', code='invalid')
        self.cleaned_data['custom'] = ex
