from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


class Job(models.Model):
    title = models.CharField(max_length=50, null=False, blank=False)
    link = models.CharField(max_length=255, null=False, blank=False)
    is_authorize = models.BooleanField(null=False, default=False)
    user_authorize = models.CharField(max_length=255, null=True, blank=True)
    password_authorize = models.CharField(max_length=255, null=True, blank=True)
    cron_expression = models.TextField(null=False, blank=True, default='* * * * *')
    is_active = models.BooleanField(null=False, default=True)
    fk_user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    save_response = models.BooleanField(null=False, default=False)
    notification_job_failed = models.BooleanField(null=False, default=False)
    notification_success_after_fail = models.BooleanField(null=False, default=False)
    deactivate_after_fail = models.BooleanField(null=False, default=False)
    created = models.DateField(null=False, blank=False, auto_now_add=True)

    def __str__(self):
        return self.title


class Response(models.Model):
    job_fail = models.BooleanField(null=False, default=False)
    response = models.TextField(null=True)
    fk_job = models.ForeignKey(Job, null=False, on_delete=models.CASCADE)

    def __str__(self):
        return self.response
